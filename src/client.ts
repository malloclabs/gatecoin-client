import crypto = require('crypto');
import https = require('https');


/* Generic "crypto exchange" interfaces. */

export interface Quote {
    symbol: string;
    timestamp: Date;
    open: number;
    bid: number;
    ask: number;
    last: number;
    volume: number;
    spread: number;
}

export interface Order {
    // TODO
}

export interface Trade {
    // TODO
}


export interface Balance {
    currency: string;
    balance: number;
    available: number;
}


export interface Client {
    connect(): Promise<Exchange>;
}

export interface Exchange {
    exchangeName: string;

    getQuote(string): Promise<Quote>;
    getAllQuotes(): Promise<{[symbol: string]: Quote}>;
    //submitOrder(Order): Promise<Trade>;         // or throw an error?
    //checkOrder(Order): Promise<Trade|null>;     // order could be in progress or complete (i.e. a Trade)
    //checkTradeStatus(Trade): Promise<XXX>;      // what does it return?

    /* Trading */
    getBalance(currency: string): Promise<Balance>;
    //getBalances(): Promise<Balance[]>;
}


/* Implementation of these interfaces for the Gatecoin exchange */

export interface GatecoinOptions {
    apiHost?:       string;
    apiBase?:       string;
    apiKey?:        string;
    apiSecret?:     string;
}


export function Gatecoin(options?: GatecoinOptions): Client {
    // TODO: validate 'options'
    return new GatecoinClient(options);
}


class GatecoinClient implements Client {

    apiHost:    string = 'api.gatecoin.com';
    apiBase:    string = '';
    apiKey:     string = null;
    apiSecret:  string = null;

    constructor(options?: GatecoinOptions) {
        if (options.apiHost)   this.apiHost = options.apiHost;
        if (options.apiBase)   this.apiBase = options.apiBase;
        if (options.apiKey)    this.apiKey = options.apiKey;
        if (options.apiSecret) this.apiSecret = options.apiSecret;
    }

    fetch(path: string, options?: {use_key?: boolean, method?: string}): Promise<any> {
        options = options || {};
        const method = options.method || 'GET';
        const fullpath = this.apiBase + path;
        // Build the API request
        const req = https.request({
            method: method,
            protocol: 'https:',
            host: this.apiHost,
            path: fullpath,
        });
        // Sign the request if we have been given an API key
        if (options.use_key) {
            if (!this.apiKey || !this.apiSecret) {
                throw new Error("Authenticated methods require an API key and secret!");
            }
            const hmac = crypto.createHmac('sha256', this.apiSecret);
            let now = (Date.now()/1000.0).toFixed(3);

            hmac.update(method.toLowerCase());
            hmac.update('https://');
            hmac.update(this.apiHost.toLowerCase());
            hmac.update(fullpath.toLowerCase());
            hmac.update(method == 'GET' ? '' : 'application/json');
            hmac.update(now);
            req.setHeader('API_PUBLIC_KEY', this.apiKey);
            req.setHeader('API_REQUEST_SIGNATURE', hmac.digest('base64'));
            req.setHeader('API_REQUEST_DATE', now);
        }
        // Fire off the request
        return new Promise((resolve, reject) => {
            req.on('response', resp => {
                const body = [];
                resp.on('error', err => reject(err));
                resp.on('data', (chunk) => { body.push(chunk); });
                resp.on('end', () => {
                    if (resp.statusCode < 200 || resp.statusCode > 299) {
                        let url = 'https://' + this.apiHost + fullpath;
                        console.log("(request error)\n  url: %s\n  statusCode: %s\n  body:\n%s", url, resp.statusCode, body.join(''));
                        reject(new Error("Failed to load URL: " + url + " (statusCode=" + resp.statusCode + ")"));
                        return;
                    }
                    const result = JSON.parse(body.join(''));
                    if (!result.responseStatus || result.responseStatus.errorCode !== undefined)
                        reject(new Error(result.responseStatus.message));
                    else
                        resolve(result);
                });
            });
            req.end();
        });
    }

    connect(): Promise<Exchange> {
        // XXX
        let promises = [];

        promises.push(this.fetch("/Reference/CurrencyPairs").then(resp => {
            let pairs = Object.create(null);  // poor man's hashmap  :)
            // XXX do we really want/need to preload all the /Reference/* data on connect?
            /*
            resp.currencyPairs.forEach(p => {
                let pair = new Pair(p);
                pairs[pair.symbol] = pair;
            });
            */
            return pairs;
        }));

        /*
         * Ping the exchange to check our authentication status.  If 'isConnected' is
         * true, our API key is valid and usable.
         *
         * TODO: actually check resp.isConnected
         */
        promises.push(this.fetch("/Ping?Message=pong", {use_key: !!this.apiSecret, method: 'POST'}));

        let exch = new GatecoinExchange(this);
        let init = Promise.all(promises);
        return init.then(() => exch);
    }

}


class GatecoinExchange implements Exchange {

    client: GatecoinClient;
    readonly exchangeName = "Gatecoin";

    constructor(client: GatecoinClient) {
        this.client = client;
    }
    
    getQuote(symbol: string): Promise<Quote> {
        const path = "/Public/LiveTicker/" + symbol;
        return this.client.fetch(path).then(resp => new GatecoinQuote(resp.ticker));
    }

    getAllQuotes(): Promise<{[symbol: string]: Quote}> {
        return this.client.fetch("/Public/LiveTicker").then(resp => {
            let quotes = {};
            resp.tickers.forEach(q => {
                let quote = new GatecoinQuote(q);
                quotes[quote.symbol] = quote;
            });
            return quotes;
        });
    }

    getBalance(currency: string): Promise<Balance> {
        return this.client.fetch("/Balance/Balances/" + currency, {use_key: true}).then(resp => {
            let b = resp.balance;
            return new GatecoinBalance(b.currency, b.balance, b.availableBalance);
        });
    }

}


class GatecoinQuote implements Quote {

    symbol: string;
    timestamp: Date;
    open: number;
    bid: number;
    ask: number;
    last: number;
    volume: number;
    spread: number;

    constructor(resp) {
        this.symbol = resp.currencyPair;
        this.timestamp = new Date(resp.createDateTime * 1000);
        this.open = resp.open;
        this.bid = resp.bid;
        this.ask = resp.ask;
        this.last = resp.last;
        this.volume = resp.volume;
        this.spread = this.ask - this.bid;
        if (this.spread < 0) {
            throw "Internal error:  ask quote is less than bid quote!";
        }
    }

    toString() {
        // XXX use the correct precision for the currency pair instead of hardcoding it to 3.
        return "<Quote('" + this.symbol + "'): open=" + this.open.toFixed(3) + ", bid=" + this.bid.toFixed(3) + ">";
    }

}


class GatecoinBalance implements Balance {

    currency: string;
    balance: number;
    available: number;

    constructor(currency: string, balance: number, available: number) {
        this.currency = currency;
        this.balance = balance;
        this.available = available;
    }

}
