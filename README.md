# gatecoin-client

Client library for accessing the Gatecoin exchange from a Node.js project.

Includes type definitions for use with TypeScript.

## Features

* Access the public exchange data (e.g. tickers, order books, and transaction history)
* Retrieve account information (requires an API key)
* Place new orders (requires an API key with `Trade` permission)

## Dependencies

None.  :)


## Contributions

All contributions are welcome.  Please make a pull request against the master branch.
